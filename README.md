# Baseline Algorithm for TIAD 2017

See the [online description](https://tiad2017.wordpress.com) of the task. We infer across dictionaries by doing a depth-first search for cycles of translations which include the desired source and target languages (German to Portuguese, Danish to Spanish, and Dutch to French).

## Requirements

* tested on Python 2.7.8
* *nix or environment like Cygwin, Babun etc with:
  * make
  * pip
* TIAD test data stored in sister folder as tiad-2017_test_data/ ([available on GitLab](https://gitlab.com/kd-public/tiad-2017_test_data))
* (Optional -- if you want to compare results to evaluation data) "gold standard" data saved in sister folder as tiad-2017_gold_standard ([available on GitLab after submission deadline](https://gitlab.com/kd-public/tiad-2017_gold_standard))

## Setup

Install requirements with
~~~~
make init
~~~~

## Running

From this directory, run
~~~~
python tiad
~~~~

The following output files are saved in output/:
* *[sourceLang]2[targetLang].tsv*: inferred translations for given language pair
* *[sourceLang]2[targetLang]_cycles.tsv*: translation cycles used for given language pair
* *stats.tsv*: Translation statistics (includes gold standard comparisons if gold standard data is present)
