class DictionaryEntry:
    headword_header = "HWord"
    pos_header = "POS"
    trans_headword_header = "TransSrc"
    trans_pos_header = "TransPOS"

    def __init__(self, language, headword, pos):
        self.language = language
        self.headword = headword
        self.pos = pos

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __hash__(self):
        return hash((self.language, self.headword, self.pos))
