import networkx as nx


def dfs_cycle(graph, source_node, target_lang):
    """
    finds cycle including source node and some node in target language
    uses DFS with constraints:
        * at most one node in each language
        * includes source node and target language
        * undirected search EXCEPT FOR BR=>DE
    returns cycle (array of DictionaryEntry nodes)
    """
    stack = [[source_node]] # stack of possible paths, extended & pruned
    while stack:
        current_path = stack[-1]
        current_node = current_path[-1]
        hit_languages = set([node.language for node in current_path])
        in_edges = graph.in_edges(current_node)
        out_edges = graph.out_edges(current_node)
        possible_continuations = [head for head, tail in in_edges] + [tail for head, tail in out_edges]
        if len(current_path) > 2 and source_node in possible_continuations and target_lang in hit_languages:
            # WE FOUND A CYCLE!!
            return current_path + [source_node]
        # don't allow reversing BR=>DE or double-dipping languages
        # (this also prevents double-backing)
        continuations = [c for c in possible_continuations if
                            (c.language not in hit_languages) and
                            (current_node.language != "de" or c.language != "br")]
        # replace current path with continuations (if they exist):
        stack.pop()
        stack.extend([current_path + [c] for c in continuations])
    raise nx.exception.NetworkXNoCycle


def generate_inferences(graph, source_lang, target_lang):
    nodes = filter(lambda x: x.language == source_lang, graph.nodes())
    cycles_found = 0
    inferred_pairs = []
    cycles = []
    for node in nodes:
        try:
            cycle = dfs_cycle(graph, node, target_lang)
            # ^ NX uses DFS to find cycles
            assert len(cycle) > 0, "Empty cycle should throw NetworkXNoCycle exception!"
            assert node in cycle, "Node absent from cycle!"
            assert cycle[0] == cycle[-1], "Cycle is not closed!"
            source_entries = filter(lambda x: x.language == source_lang, cycle[:-1])
            target_entries = filter(lambda x: x.language == target_lang, cycle[:-1])
            assert len(source_entries) == len(target_entries) == 1
            source_entry = source_entries[0]
            target_entry = target_entries[0]
            inferred_pairs.append((source_entry, target_entry))
            cycles.append(cycle)
        except nx.exception.NetworkXNoCycle:
            pass
    return inferred_pairs, cycles
