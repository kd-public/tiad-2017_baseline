if __name__ == "__main__":

    from import_graph import build_graph
    from export_data import export_inferences, export_statistics
    from generate_inferences import generate_inferences

    # task is to infer dictionaries DE=>BR, DK=>ES, and NL=>FR
    goals = [("de", "br"), ("dk", "es"), ("nl", "fr")]

    graph = build_graph()
    inferences = {(source_lang, target_lang): generate_inferences(graph, source_lang, target_lang) \
                  for source_lang, target_lang in goals}
    export_inferences(inferences)
    export_statistics(inferences, graph)