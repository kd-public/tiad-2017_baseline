import os, csv
from dictionary_entry import DictionaryEntry

if os.path.basename(os.getcwd()) == "tiad":
    # running from IDE
    data_dir = os.path.join("..", "..", "tiad-2017_gold_standard")
else:
    data_dir = os.path.join("..", "tiad-2017_gold_standard")

filename = os.path.join(data_dir, "gold.tsv")


def gold_exists():
    return os.path.isdir(data_dir) and os.path.isfile(filename)


def get_gold():
    with open(filename) as tsvfile:
        rows = [x for x in csv.reader(tsvfile, delimiter="\t")]
    gold_dict = {}
    for row in rows:
        hw, pos, lang, hw2, pos2, lang2 = row
        head = DictionaryEntry(language=lang, headword=hw, pos=pos)
        tail = DictionaryEntry(language=lang2, headword=hw2, pos=pos2)
        if head in gold_dict:
            if tail not in gold_dict[head]:
                gold_dict[head].append(tail)
        else:
            gold_dict[head] = [tail]
    return gold_dict
