import os, csv
import networkx as nx
from dictionary_entry import DictionaryEntry

if os.path.basename(os.getcwd()) == "tiad":
    # running from IDE
    data_dir = os.path.join("..", "..", "tiad-2017_test_data")
else:
    data_dir = os.path.join("..", "tiad-2017_test_data")

subdirs = ["DE_DK_FR_ES_BR_DE", "DE_EN_BR_DE", "DE_JA_ES_BR_DE", "DE_NL_ES_DK_FR_BR_DE"]
supported_langs = {"de", "dk", "fr", "es", "br", "ja", "nl", "en"}

required_headers = {DictionaryEntry.headword_header, DictionaryEntry.pos_header,
                        DictionaryEntry.trans_headword_header, DictionaryEntry.trans_pos_header}


# get languages and filenames in subdirectory
def _get_subdir_info(subdir):
    path = os.path.join(data_dir, subdir)
    langs = subdir.lower().split("_")
    assert set(langs).issubset(supported_langs), "Unsupported language(s): %s!" % set(langs).difference(supported_langs)
    assert langs[0] == langs[-1], "Language path not closed: %s!" % langs
    links = zip(langs, langs[1:]) # [("de", "en"), ("en", "br"), ...]
    link_filenames = map(lambda (x, y): os.path.join(path, "%s-%s%s.tsv" % (subdir.lower(), x, y)), links)
    assert all(map(os.path.isfile, link_filenames)), "Bad filename in directory %s!" % path
    return langs, links, link_filenames


# collect all dictionary entries from links in subdirectory, add to graph and hashtable
def _collect_entries(subdir, graph):
    langs, links, link_filenames = _get_subdir_info(subdir)
    for link, link_fn in zip(links, link_filenames):
        source_lang, target_lang = link
        with open(link_fn) as tsvfile:
            rows = [x for x in csv.reader(tsvfile, delimiter="\t")]
        header = rows[0]
        data = rows[1:]
        assert required_headers.issubset(set(header)), "Missing required header(s) in file: %s!" % link_fn
        for data_row in data:
            # if missing entries @ end, have to bad with empty str for correct indexing
            padded_data_row = data_row + [""] * max(0, len(header) - len(data_row))
            h2d = {h: d for h, d in zip(header, padded_data_row)}

            source_entry = DictionaryEntry(language=source_lang,
                                           headword=h2d[DictionaryEntry.headword_header],
                                           pos=h2d[DictionaryEntry.pos_header])
            target_entry = DictionaryEntry(language=target_lang,
                                           headword=h2d[DictionaryEntry.trans_headword_header],
                                           pos=h2d[DictionaryEntry.trans_pos_header])

            graph.add_edge(source_entry, target_entry)


def build_graph():
    graph = nx.DiGraph()
    for subdir in subdirs:
        _collect_entries(subdir, graph)
    return graph
