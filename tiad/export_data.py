import os, csv
from import_gold_standard import gold_exists, get_gold

if os.path.basename(os.getcwd()) == "tiad":
    # running from IDE
    out_dir = os.path.join("..", "output")
else:
    out_dir = "output"


def export_inference_pairs(source_lang, target_lang, pairs):
    gold_dict = get_gold() if gold_exists() else []

    filename = os.path.join(out_dir, "%s2%s.tsv" % (source_lang, target_lang))
    with open(filename, "wb") as tsvfile:
        mywriter = csv.writer(tsvfile, delimiter="\t")
        if gold_dict:
            mywriter.writerow(["SourceHW", "SourcePOS", "TargetHW", "TargetPOS", "SourceInGold", "GoldTransHW", "GoldTransPOS", "(more gold translations)"])
        else:
            mywriter.writerow(["SourceHW", "SourcePOS", "TargetHW", "TargetPOS"])
        for pair in pairs:
            source_entry, target_entry = pair
            if gold_dict:
                if source_entry in gold_dict:
                    gold_entries = [x for y in [(e.language, e.headword) for e in gold_dict[source_entry]] for x in y]
                else:
                    gold_entries = []
                mywriter.writerow([source_entry.headword, source_entry.pos, target_entry.headword, target_entry.pos,
                                   source_entry in gold_dict] + gold_entries)
            else:
                mywriter.writerow([source_entry.headword, source_entry.pos, target_entry.headword, target_entry.pos])


def export_cycles(source_lang, target_lang, cycles):
    filename = os.path.join(out_dir, "%s2%s_cycles.tsv" % (source_lang, target_lang))
    with open(filename, "wb") as tsvfile:
        mywriter = csv.writer(tsvfile, delimiter="\t")
        for cycle in cycles:
            mywriter.writerow([x for y in [(e.language, e.headword) for e in cycle] for x in y])


def export_inferences(inferences):
    # inferences: (source_lang, target_lang) => inference_pairs, cycles
    for source_lang, target_lang in inferences:
        inference_pairs, cycles = inferences[source_lang, target_lang]
        export_inference_pairs(source_lang, target_lang, inference_pairs)
        export_cycles(source_lang, target_lang, cycles)


def export_statistics(inferences, graph):
    # inferences: (source_lang, target_lang) => inference_pairs, cycles

    filename = os.path.join(out_dir, "stats.tsv")

    with open(filename, "wb") as tsvfile:
        mywriter = csv.writer(tsvfile, delimiter="\t")

        if gold_exists():
            gold_dict = get_gold()
            mywriter.writerow(["source", "target", "source_nodes", "source_nodes_in_gold", "inferences",
                               "inferences_in_gold", "denominator", "estimated_precision", "estimated_recall",
                               "estimated_f_measure"])
        else:
            gold_dict = []
            mywriter.writerow(["source", "target", "source_nodes", "inferences"])

        nodes = graph.nodes()
        for source_lang, target_lang in inferences:
            inference_pairs, cycles = inferences[source_lang, target_lang]
            source_nodes = [n for n in nodes if n.language == source_lang]

            n_source_nodes = len(source_nodes)
            n_inferences = len(inference_pairs)

            if gold_dict:
                n_source_in_gold = len([1 for node in source_nodes if node in gold_dict])
                n_inferences_in_gold = len([1 for head, tail in inference_pairs
                                            if head in gold_dict and tail in gold_dict[head]])

                denominator = 100 if (source_lang, target_lang) == ("de", "br") else n_source_nodes

                precision = n_inferences_in_gold * 1. / n_inferences
                recall = n_inferences * 1. / denominator
                f_measure = 2 * (precision * recall) / (precision + recall)

                mywriter.writerow([source_lang, target_lang, n_source_nodes, n_source_in_gold, n_inferences,
                                   n_inferences_in_gold, denominator, precision, recall, f_measure])
            else:
                mywriter.writerow([source_lang, target_lang, n_source_nodes, n_inferences])
